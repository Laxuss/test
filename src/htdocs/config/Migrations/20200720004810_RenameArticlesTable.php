<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class RenameArticlesTable extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
    	$this->table('articles')
    	->rename('tweets')
    	->save();
    }

    /**
     * Down Method.
     *
     * @return void
     */
    public function down()
    {
    	$this->table('tweets')
    	->rename('articles')
    	->save();
    }
}
