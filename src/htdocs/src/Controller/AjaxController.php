<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Class AjaxController.
 *
 * @package App\Controller
 */
class AjaxController extends AppController
{
    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Tweets');
        $this->setAuth();
    }

    /**
     * Add image into object.
     *
     * @param object $object Object.
     * @param object $image  Image object.
     * @return object
     */
    public function image($object, $image)
    {
        $name = $image->getClientFilename();
        if ($name) {
            $img = time() . '-' . $name;
            $targetPath = WWW_ROOT . 'img' . DS . $img;
            $image->moveTo($targetPath);
            $object->image = $img;
        }

        return $object;
    }

    /**
     * Add tweet method.
     *
     * @return \Cake\Http\Response|null Renders view otherwise, return type json.
     */
    public function add()
    {
        $user = $this->Authentication->getResult()->getData();
        $tweet = $this->Tweets->newEntity($this->request->getData());
        $tweet->user_id = $user->id;

        if ($tweet->getErrors()) {
            $errors = ['errors' => $tweet->getErrors()];

            return $this->response->withType('application/json')->withStringBody(json_encode($errors));
        }
        $image = $this->request->getData('imageFile');
        $this->image($tweet, $image);

        if ($this->Tweets->save($tweet)) {
            $this->set(compact('tweet'));
            $response = $this->render('/Ajax/tweet');
            $jsonResponse = ['html' => $response->__debugInfo()['body']];

            return $response->withType('application/json')->withStringBody(json_encode($jsonResponse));
        }
    }

    /**
     * Scroll ajax method for Home index.
     *
     * @return \Cake\Http\Response|null Renders view otherwise.
     */
    public function scrollHome()
    {
        $query = $this->Tweets->find('tweets')->select($this->Tweets->Users);
        $tweets = $this->paginate($query, ['limit' => $this->limitPage()]);
        $this->set(compact('tweets'));
        $response = $this->render('/element/tweet');
        $jsonResponse = ['html' => $response->__debugInfo()['body']];

        return $response->withType('application/json')->withStringBody(json_encode($jsonResponse));
    }

    /**
     * Scroll ajax method for Tweets profile and index.
     *
     * @param int|null $id User id.
     * @return \Cake\Http\Response Renders view otherwise.
     */
    public function scrollProfile($id = null)
    {
        $query = $this->Tweets->find('tweets')->select($this->Tweets->Users)->where(['Users.id' => $id]);
        $tweets = $this->paginate($query, ['limit' => $this->limitPage()]);
        $this->set(compact('tweets'));
        $response = $this->render('/element/tweet');
        $jsonResponse = ['html' => $response->__debugInfo()['body']];

        return $response->withType('application/json')->withStringBody(json_encode($jsonResponse));
    }
}
