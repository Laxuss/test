<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;

/**
 * Class HomeController.
 *
 * @package App\Controller
 */
class HomeController extends AppController
{
    /**
     * Event which occurs before every action in the controller.
     *
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('master');
        $this->setAuth();
        $this->loadModel('Tweets');
    }

    /**
     * Index method.
     *
     * @return \Cake\Http\Response|null|void Renders view.
     */
    public function index()
    {
        $query = $this->Tweets->find('tweets')->select($this->Tweets->Users);
        $tweets = $this->paginate($query, ['limit' => $this->limitPage()]);
        $this->set(compact('tweets'));
    }
}
