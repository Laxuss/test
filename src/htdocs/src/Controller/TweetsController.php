<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;

/**
 * Tweets Controller
 *
 * @property \App\Model\Table\TweetsTable $Tweets
 * @method \App\Model\Entity\Tweet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TweetsController extends AppController
{
    /**
     * Event which occurs before every action in the controller.
     *
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('master');
        $this->setAuth();
    }

    /**
     * Index method.
     *
     * @return \Cake\Http\Response|null|void Renders view.
     */
    public function index()
    {
        $user = $this->Authentication->getResult()->getData();
        $query = $this->Tweets->find('tweets')->select($this->Tweets->Users)->where(['Users.id' => $user->id]);
        $tweets = $this->paginate($query, ['limit' => $this->limitPage()]);
        $this->set(compact('tweets'));
    }

    /**
     * Delete method.
     *
     * @param string|null $id Tweet id.
     * @return \Cake\Http\Response|null Renders view.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'ajax']);
        $user = $this->Authentication->getResult()->getData();
        $tweet = $this->Tweets->get($id);
        if ($tweet->image) {
            $imgpath = 'img/' . $tweet->image;
            if (file_exists($imgpath)) {
                unlink($imgpath);
            }
        }
        if ($tweet->user_id == $user->id) {
            $this->Tweets->delete($tweet);
        }
        $success = ['success' => __('The tweet has been deleted.')];

        return $this->response->withType('application/json')->withStringBody(json_encode($success));
    }

    /**
     * View tweets by user id.
     *
     * @param int|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view.
     */
    public function profile($id = null)
    {
        $query = $this->Tweets->find('tweets')->select($this->Tweets->Users)->where(['Users.id' => $id]);
        $tweets = $this->paginate($query, ['limit' => $this->limitPage()]);
        $this->set(compact('tweets', 'id'));
    }
}
