<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Security;

/**
 * Users Controller.
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    /**
     * Event which occurs before every action in the controller.
     *
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('master');
    }

    /**
     * Login method.
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $user = $result->getData();
            if ($user->email_status == $this->noActiveStatus()) {
                $this->Authentication->logout();

                return $this->redirect(['action' => 'reconfirm', $user->remember_token]);
            }

            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }

    /**
     * Reconfirm email method.
     *
     * @param string|null $token Remember token.
     * @return \Cake\Http\Response|null
     */
    public function reconfirm($token = null)
    {
        $result = $this->Authentication->getResult();
        $user = $this->Users->find('all')->where(['remember_token' => $token])->first();
        if ($result->isValid() || is_null($user)) {
            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        }
        $user->remember_token = Security::hash(Security::randomString(64));
        $this->Users->save($user);
        $this->getMailer('User')->send('verify', [$user]);
        $this->Flash->success(__('Your confirmation email has been sent.'));
        $this->set(compact('user'));
    }

    /**
     * Logout method.
     *
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->Authentication->logout();

        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }

    /**
     * Register method.
     *
     * @return \Cake\Http\Response|null|void Redirects on successful register, renders view otherwise.
     */
    public function register()
    {
        $user = $this->Users->newEmptyEntity();
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        }
        if ($this->request->is('post')) {
            $token = Security::hash(Security::randomString(64));
            $user->remember_token = $token;
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Register successfully, your confirmation email has been sent.'));
                $this->getMailer('User')->send('verify', [$user]);

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Confirmation email method.
     *
     * @param string|null $token Remember token.
     * @return \Cake\Http\Response|null
     */
    public function verification($token = null)
    {
        $user = $this->Users->find('all')->where(['remember_token' => $token])->first();
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        }
        if (!is_null($user)) {
            $user->email_status = $this->activeStatus();
            $user->remember_token = null;
            if (!$this->Users->save($user)) {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->Flash->success(__('You have already received the email, please log in.'));

        return $this->redirect(['action' => 'login']);
    }
}
