<?php
declare(strict_types=1);

namespace App\Mailer;

use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{
    /**
     * Config mail.
     *
     * @param object $user User.
     * @return void
     */
    public function verify($user)
    {
        $this
            ->setTransport('mailtrap')
            ->setEmailFormat('html')
            ->setSubject('Please confirm your email to activation your account.')
            ->setTo($user->email)
            ->setViewVars(['user' => $user])
            ->viewBuilder()
            ->setTemplate('default')
            ->setLayout('email');
    }
}
