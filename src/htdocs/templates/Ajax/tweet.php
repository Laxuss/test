<div class="card-body article-body px-lg-4 pt-lg-0 pb-lg-3 px-3 py-1 mt-2" id="tweetContent-<?= $tweet->id ?>">
    <div class="card-title form-inline">
        <?= $this->element('headtweet') ?>
    </div>
    <pre class="cards-text pre-text mb-2" id="pre-<?= $tweet->id ?>"><?= h($tweet->content) ?></pre>

    <? if ($tweet->image): ?>
        <?= $this->Html->image($tweet->image,
            ['class' => 'tweet-image', 'alt' => 'Twitter-' . $tweet->id]) ?>
        <div id="modalArea-<?= $tweet->id ?>" class="modal-area">
            <div class="modal-main">
                <div class="modal-flex">
                    <span class="close">&times;</span>
                    <div class="modal-image">
                        <img class="image-content" id="imgAvatar-<?= $tweet->id ?>">
                    </div>
                    <div class="modal-tweet">
                        <div class="head-tweet">
                            <?= $this->element('headtweet') ?>
                        </div>
                        <pre class="cards-text pre-text ml-2 mt-2"
                             id="pre-<?= $tweet->id ?>"><?= h($tweet->content) ?></pre>
                    </div>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>
