<?php
/**
 * @var \App\View\AppView                                              $this
 * @var \App\Model\Entity\Tweet[]|\Cake\Collection\CollectionInterface $tweets
 * @var \App\Controller                                                $user
 */
?>
<div class="left-body px-lg-3">
    <div class="col-12 pt-2 pb-5 article-area" id="tweet">
        <h5 class="pt-2 pb-3 head-home"><b><?= $this->Html->link(__('Home'),
                    ['controller' => 'home', 'action' => 'index']) ?></b></h5>
        <div class="card card-outline-secondary form-article mt-2">
            <?= $this->element('tweets/createtweet') ?>
        </div>

        <?= $this->element('tweet') ?>

        <ul class="pagination mt-3 pagination-home" total-page="<?= $this->Paginator->total() ?>">
            <?= $this->Paginator->next(__('See more')) ?>
        </ul>
    </div>
</div>
