<div class="px-lg-5 mt-5 pt-5">
    <div class="px-lg-5 mx-lg-5 px-3">
        <?= $this->Flash->render() ?>
        <div class="card card-outline-secondary">
            <div class="card-header">
                <h3 class="mb-0"><?= __('Login') ?></h3>
            </div>
            <div class="card-body">
                <?= $this->Form->create(null, ['id' => 'formLogin']) ?>
                <div class="form-group">
                    <?= $this->Form->control('email', ['class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('password', ['class' => 'form-control']) ?>
                </div>
                <?= $this->Form->submit(__('Login'), ['class' => 'btn btn-primary px-4 mt-3 btn-login']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
