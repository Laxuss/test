<div class="mt-5 pt-5">
    <?= $this->Flash->render() ?>
</div>
<div class="card mt-5">
    <div class="card-body">
        <h4><?= __('Before proceeding, please check your email for a verification link. If you did not receive the email, click
            here to request another.') ?></h4>
        <?= $this->Html->link(__('Reconfirm'), ['action' => 'reconfirm', $user->remember_token],
            ['class' => 'btn btn-success']); ?>
    </div>
</div>
