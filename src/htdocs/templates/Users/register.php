<?php
/**
 * @var \App\View\AppView      $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="px-lg-5 mt-5 pt-5">
    <div class="px-lg-5 mx-lg-5 px-3">
        <?= $this->Flash->render() ?>
        <div class="card card-outline-secondary">
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <h3 class="mb-0"><?= __('Register') ?></h3>
                </div>
                <div class="card-body">
                    <?= $this->Form->create($user, ['id' => 'formSignup']) ?>
                    <div class="form-group">
                        <?= $this->Form->control('username', ['class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('email', ['class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control('password', ['class' => 'form-control']) ?>
                    </div>
                    <?= $this->Form->button(__('Sign up'), ['class' => 'btn btn-primary px-4 mt-3']) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
