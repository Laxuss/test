<div class="mt-5 pt-5">
    <?= $this->Flash->render() ?>
</div>
<div class="card mt-4">
    <div class="card-body">
        <h4><?= __('Your email has been verified, and please login now.') ?></h4>
        <?= $this->Html->link(__('Login here'), ['action' => 'login']); ?>
    </div>
</div>
