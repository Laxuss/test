<?= $this->Form->create($tweets, ['type' => 'file']) ?>
<div class="card-header form-inline py-1 tweet-header">
    <?= $this->Html->image('default-avatar.png',
        ['class' => 'rounded-circle default-avatar']) ?>
    <?= $this->Html->link($user->username,
        ['controller' => 'Tweets', 'action' => 'index'], ['class' => 'nav-link user-link']) ?>
    <div class="ml-auto">
        <?= $this->Form->button(__('Tweet'),
            ['class' => 'btn btn-primary px-2 py-0 btn-tweet', 'disabled']) ?>
    </div>
</div>
<div class="card-body p-2">
    <div class="card-block">
        <div class="tab-content">
            <div class="tab-pane active" id="home" role="tabpanel">
                <?= $this->Form->textarea('content',
                    [
                        'rows' => '1',
                        'class' => 'form-control border-0 sinborde area-body',
                        'placeholder' => __("What's on your mind?")
                    ])
                ?>
            </div>
        </div>
    </div>
    <div class="upload-btn-wrapper mt-2 ml-2 pl-1">
        <button class="btn-upload">Upload image</button>
        <input type="file" name="imageFile" id="imageFile"/>
    </div>
</div>
<?= $this->Form->end() ?>
