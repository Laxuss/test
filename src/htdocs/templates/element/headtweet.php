<?= $this->Html->image('default-avatar.png',
    ['class' => 'rounded-circle default-avatar image-avatar']) ?>
<div class="right-avatar">
    <div class="user-text">
        <?= $this->Html->link($user->username,
            ['controller' => 'Tweets', 'action' => 'index'], ['class' => 'user-link']) ?>
    </div>
    <div class="diff-time">
        <?= $tweet->created->diffForHumans(null, true) ?>
    </div>
</div>
<div class="ml-auto pr-2">
    <?= $this->element('actiontweet', ['tweet' => $tweet]) ?>
</div>
