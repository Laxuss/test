<div class="form-body fixed-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $this->Html->link($this->Html->image('twitter2.png', ['width' => '40', 'class' => 'mr-lg-4 mr-2']),
                ['controller' => 'Home', 'action' => 'index'], ['escape' => false]) ?>
            <?php if (isset($this->request->getAttribute('authentication')->getResult()->getData()->username)): ?>
                <div class="form-inline ml-auto">
                    <?= $this->Html->image('default-avatar.png',
                        ['class' => 'rounded-circle default-avatar']) ?>
                    <?= $this->Html->link($user->username,
                        ['controller' => 'Tweets', 'action' => 'index'], ['class' => 'nav-link user-link']) ?>
                    <div class="dropdown mb-1">
                        <a class="dropdown-toggle" href="#" role="button" id="logoutDropdown" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="logoutDropdown">
                            <a class="dropdown-item"
                               href="<?= $this->Url->build([
                                   'controller' => 'Users',
                                   'action' => 'logout'
                               ]) ?>"><?= __('Logout') ?></a>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if (!isset($this->request->getAttribute('authentication')->getResult()->getData()->username)): ?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent"
                        aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
                        <li class="nav-item mt-1 mx-1">
                            <a class="nav-link signin-link py-1 px-3"
                               href="<?= $this->Url->build([
                                   'controller' => 'Users',
                                   'action' => 'login'
                               ]) ?>"><?= __('Login') ?></a>
                        </li>
                        <li class="nav-item mt-1 mx-1">
                            <a class="nav-link signup-link py-1 px-3" href="<?= $this->Url->build([
                                'controller' => 'Users',
                                'action' => 'register'
                            ]) ?>"><?= __('Register') ?></a>
                        </li>
                    </ul>
                </div>
            <?php endif ?>
        </nav>
    </div>
</div>
