<? foreach ($tweets as $tweet): ?>
    <div class="card-body article-body px-lg-4 pt-lg-0 pb-lg-3 px-3 py-1 mt-2" id="tweetContent-<?= $tweet->id ?>">
        <div class="card-title form-inline">
            <?= $this->Html->image('default-avatar.png',
                ['class' => 'rounded-circle default-avatar image-avatar']) ?>

            <div class="right-avatar" id="rightAvater-<?= $tweet->user->id ?>">
                <?= $this->element('tweets/rightavatar', ['tweet' => $tweet, 'user' => $user]) ?>
            </div>

            <div class="ml-auto">
                <?= $this->element('tweets/actiontweet', ['tweet' => $tweet]) ?>
            </div>

        </div>
        <pre class="cards-text pre-text mb-2" id="pre-<?= $tweet->id ?>"><?= h($tweet->content) ?></pre>

        <!--Modal image-->
        <?= $this->element('tweets/modalimage', ['tweet' => $tweet, 'user' => $user]) ?>
    </div>
<? endforeach ?>
<span class="page-current" data-page="<?= $this->Paginator->counter('{{page}}'); ?>"></span>
