<div class="drop-tweet dropdown show pl-2">
    <a class="drop-tweet" href="#" role="button" id="dropAction" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false">…
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropAction">

        <? if (!isset($tweet->user) || $tweet->user->id === $user->id): ?>
            <a class="dropdown-item" href="#" data-toggle="modal"
               data-target="#deleteTweet-<?= $tweet->id ?>"><?= __('Delete Tweet') ?></a>
        <? endif; ?>

        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#"><?= __('Action Tweet') ?></a>
    </div>

    <!-- Delete modal -->
    <div class="modal fade" id="deleteTweet-<?= $tweet->id ?>" tabindex="-1" role="dialog"
         aria-labelledby="deleteLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?= __('Are you sure you want to delete this tweet?') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <?= $this->Form->create(null, [
                        'url' => ['controller' => 'Tweets', 'action' => 'delete', $tweet->id],
                        'class' => 'tweet-delete'
                    ]) ?>
                    <?= $this->Form->button(__('Delete'),
                        ['class' => 'btn btn-primary', 'id' => 'btnDelete' . $tweet->id]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
