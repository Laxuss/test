<? if ($tweet->image): ?>
    <?= $this->Html->image($tweet->image,
        ['class' => 'tweet-image', 'alt' => 'Twitter-' . $tweet->id]) ?>
    <div id="modalArea-<?= $tweet->id ?>" class="modal-area">
        <div class="modal-main">
            <div class="modal-flex">
                <span class="close">&times;</span>
                <div class="modal-image">
                    <img class="image-content" id="imgAvatar-<?= $tweet->id ?>">
                </div>
                <div class="modal-tweet">
                    <div class="head-tweet">
                        <?= $this->Html->image('default-avatar.png',
                            ['class' => 'rounded-circle default-avatar image-avatar']) ?>

                        <div class="right-avatar">
                            <?= $this->element('tweets/rightavatar', ['tweet' => $tweet, 'user' => $user]) ?>
                        </div>

                        <div class="ml-auto pr-2">
                            <?= $this->element('tweets/actiontweet', ['tweet' => $tweet]) ?>
                        </div>

                    </div>
                    <pre class="cards-text pre-text ml-2 mt-2"
                         id="pre-<?= $tweet->id ?>"><?= h($tweet->content) ?></pre>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
