<div class="user-text">

    <? if (!isset($tweet->user) || $tweet->user->id === $user->id): ?>
        <?= $this->Html->link($tweet->user->username,
            ['controller' => 'Tweets', 'action' => 'index'], ['class' => 'user-link']) ?>
    <? else: ?>
        <?= $this->Html->link($tweet->user->username,
            ['controller' => 'Tweets', 'action' => 'profile', $tweet->user->id], ['class' => 'user-link']) ?>
    <? endif; ?>

</div>
<div class="diff-time">
    <?= $tweet->created->diffForHumans(null, true) ?>
</div>
