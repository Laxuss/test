<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title>Email</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-5">
                <div class="card-body">
                    <h3><b>Hello!<?= $user->username ?></b></h3>
                    <h5 class="card-title my-4"><?= __('Please click the button below to verify your email address.') ?></h5>
                    <a class="btn btn-primary""
                    href="<?= Cake\Routing\Router::url("/", true) . $this->Url->build([
                        'controller' => 'Users',
                        'action' => 'verification',
                        $user->remember_token
                    ]) ?>"><?= __('Verification Email') ?></a>
                    <h5 class="card-title my-4"><?= __('If you did not create an account, no further action is required.') ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
