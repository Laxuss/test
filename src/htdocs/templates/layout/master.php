<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Twitter</title>
    <?= $this->element('style') ?>
</head>
<body>
<?= $this->element('nav') ?>
<div class="container px-0 tweet-container">
    <?= $this->fetch('content') ?>
</div>
<?= $this->element('script') ?>
</body>
</html>
