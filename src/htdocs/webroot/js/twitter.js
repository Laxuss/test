$(document).ready(function () {
    let action = new Action();
    let ajax = new Ajax();
    let validate = new Validate();
    validate.login();
    validate.register();
    action.showTextarea();
    action.truncateVertical(5);
    action.disabledBtnTweet();
    action.modalImage();
    ajax.addTweet();
    ajax.scroll('scrollHome');
    ajax.scroll('scrollProfile');
    ajax.deleteTweet();
});

function Validate() {
    /**
     * Login validate.
     */
    this.login = function () {
        $('#formLogin').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                }
            },
            messages: {
                email: {
                    required: "Email cannot be left empty",
                    email: "Invalid email format",
                },
                password: {
                    required: "Password cannot be left empty",
                    minlength: "Password need to be at least 6 characters long",
                }
            }
        });
    }

    /**
     * Register validate.
     */
    this.register = function () {
        $('#formSignup').validate({
            rules: {
                username: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                }
            },
            messages: {
                username: {
                    required: "Username cannot be left empty"
                },
                email: {
                    required: "Email cannot be left empty",
                    email: "Invalid email format",
                },
                password: {
                    required: "Password cannot be left empty",
                    minlength: "Password need to be at least 6 characters long",
                }
            }
        });
    }
}

function Action() {
    /**
     * Textarea function.
     */
    this.showTextarea = function () {

        // When blur on the textarea.
        $('.area-body').blur(function () {
            $(this).removeAttr('style').attr('rows', 1);
        });

        // When write on the textarea.
        $('.area-body').on('input', function () {
            $(this).css({'height': 'auto'});
            let height = $(this)[0].scrollHeight + 'px';
            $(this).css({'height': height});
        });

        // When click on the textarea.
        $('.area-body').on('click', function () {
            let height = $(this)[0].scrollHeight + 'px';
            $(this).css({'height': height});
            $('.error-message').remove();
        });

        // Remove required input end textarea.
        $('textarea').removeAttr('required').removeAttr('maxlength');
        $('.page-current').hide();

        setTimeout(function () {
            $('.alert').hide();
        }, 5000);
    }

    /**
     * Truncate text vertical.
     *
     * @param row Line number of tweets.
     */
    this.truncateVertical = function (row) {
        let line = parseInt($('.pre-text').css('line-height'));
        $('.pre-text').each(function () {
            let id = $(this).attr('id').split('-')[1];
            let ending = '<span class="ending" id="end-' + id + '">... <a href="#">See More</a></span>';
            let height = $(this)[0].scrollHeight;

            // If height > line * row then add style height = line * row.
            if (height > line * row) {
                $('#end-' + id).hide();
                $(this).css({'height': line * row, 'overflow': 'hidden'}).after(ending);
            }

            // If click see more then hide see more.
            $(document).on('click', '#end-' + id, function (event) {
                event.preventDefault();
                $('#pre-' + id).css({'height': height});
                $(this).hide();
            });
        });
    }

    /**
     * Disabled Button Tweet.
     */
    this.disabledBtnTweet = function () {
        $(document).keyup('.area-body', function () {
            let content = $.trim($('.area-body').val());
            if (content.length > 0) {
                $('.btn-tweet').attr('disabled', false);
            } else {
                $('.btn-tweet').attr('disabled', true);
            }
        });
    }

    /**
     * Modal image.
     */
    this.modalImage = function () {
        // Show modal image.
        $(document).on('click', '.tweet-image', function (event) {
            let src = $(this).attr('src');
            let alt = $(this).attr('alt');
            let id = alt.split('-')[1];
            $('#modalArea-' + id).css({'display': 'block'});
            $('#imgAvatar-' + id).attr('src', src);
            history.pushState(null, null, src);

            //Disable scroll.
            $('html, body').css({
                overflow: 'hidden'
            });
            $('.form-body').hide();
        })

        // Hide modal image.
        $(document).on('click', '.close', function (event) {
            $('.modal-area').css({'display': 'none'});
            history.back();

            $('html, body').css({
                overflow: 'auto',
                height: 'auto'
            });
            $('.form-body').show();
        })
    }
}

function Ajax() {
    /**
     * Add tweets ajax.
     */
    this.addTweet = function () {
        $(document).on('submit', '.form-article form', function (event) {
            event.preventDefault();
            let image = $('#imageFile').val();
            $('.error-message').remove();

            // Check validate image.
            if (image != '') {
                let size = $('#imageFile')[0].files[0].size;
                let extension = image.split('.').pop().toUpperCase();

                // Check file extension.
                if (extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG" && extension != '') {
                    html = "<div class='error-message pl-3 pt-2'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>"
                    $('.tweet-header').after(html);

                    return false;
                }

                // Check file size.
                if (size > 1000000) {
                    html = "<div class='error-message pl-3 pt-2'>Image file size must be less than 1MB.</div>"
                    $('.tweet-header').after(html);

                    return false;
                }
            }

            // Ajax add tweet.
            $.ajax({
                url: '/ajax/add',
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", $('[name="_csrfToken"]').val());
                    $('.btn-tweet').attr('disabled', true);
                },
            }).done(function (data) {
                if (data.errors) {
                    $.each(data.errors.content, function (key, value) {
                        html = "<div class='error-message pl-3 pt-2' id='errorMess'>" + value + "</div>";
                    });
                    $('.tweet-header').after(html);
                } else {
                    let total = $('ul.pagination.mt-3.pagination-articles').attr('total-page');
                    let page = $('.page-current').last().attr('data-page');

                    // If total page > 1 then hidden div element with class article-body final.
                    if (total > page) {
                        setTimeout(function () {
                            $('div.article-body').last().remove();
                        }, 1500);
                    }

                    // Set timeout add.
                    setTimeout(function () {
                        $('.area-body').val('');
                        $('input[type=file]').val('');
                        $('.error-message').remove();
                        $('.form-article').after(data.html);

                        let action = new Action();
                        action.truncateVertical(5);
                    }, 1500);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert('Custom message. Error: ' + errorThrown);
            });
        });
    }

    /**
     * Scroll page ajax.
     *
     * @param action Action ajax from AjaxController.
     */
    this.scroll = function (action) {
        if (action === 'scrollHome') {
            var total = $('ul.pagination.mt-3.pagination-home').attr('total-page');
        }
        if (action === 'scrollProfile' && typeof ($('.right-avatar').attr('id')) !== 'undefined') {
            var id = $('.right-avatar').attr('id').split('-').pop();
            var total = $('ul.pagination.mt-3.pagination-view').attr('total-view');
            action += '/' + id;
        }
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                let page = $('a.page-link').attr('href').split('page=')[1];
                let href = $('a.page-link').attr('href').split('page=')[0];
                if (Number(page) <= total) {
                    $.ajax({
                        url: "/ajax/" + action + "?page=" + page
                    }).done(function (data) {
                        href += 'page=' + (Number(page) + 1);
                        $('a.page-link').attr('href', href);
                        $('ul.pagination.mt-3').before(data.html);

                        // Truncate text.
                        let action = new Action();
                        action.truncateVertical(5);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        alert('Custom message. Error: ' + errorThrown);
                    });
                }
            }
        });
    }

    /**
     * Delete tweet ajax.
     */
    this.deleteTweet = function () {
        $(document).on('submit', '.tweet-delete', function (event) {
            event.preventDefault();
            let action = $(this).attr('action');
            let id = action.split('/').pop();
            $.ajax({
                url: action,
                method: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRF-Token", $('[name="_csrfToken"]').val());
                    $('#btnDelete' + id).attr('disabled', true).text('Delete...');
                },
            }).done(function (data) {
                setTimeout(function () {
                    $('#tweetContent-' + id).hide();
                    $('#deleteTweet-' + id).modal('hide');
                    let action = new Action();
                    action.truncateVertical(5);
                }, 1000);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert('Custom message. Error: ' + errorThrown);
            });
        });
    }
}

